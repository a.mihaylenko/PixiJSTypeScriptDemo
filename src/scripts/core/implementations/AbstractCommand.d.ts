declare namespace core.implementations {
    import ICommand = core.interfaces.ICommand;
    import IModel = core.interfaces.IModel;
    import IView = core.interfaces.IView;
    abstract class AbstractCommand implements ICommand {
        protected _model: IModel;
        protected _view: IView;
        constructor(model: IModel, view: IView);
        abstract execute(): void;
        abstract dispose(): void;
    }
}
