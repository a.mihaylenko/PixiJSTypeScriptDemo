import IAppContext = core.interfaces.IAppContext;
import ICommand = core.interfaces.ICommand;
namespace core.implementations
{
    export abstract class AbstractController implements IController
    {

        protected abstract get appContext(): IAppContext;
        protected abstract set appContext(value: IAppContext);

        private _commandMap:Map<string, Function>;
        private _commandsInMemory:Map<string,ICommand>;

        constructor() {
            this._commandMap = new Map<string, Function>();
            this._commandsInMemory = new Map<string, ICommand>();
        }


        /**
         *@inheritDoc
         */
        public registerCommand(name:string,classImpl:Function): void
        {
            if (this.hasCommand(name))
                throw new Error("Error register new command with name:" + name +", command already registered");
            this._commandMap.set(name, classImpl);
        }

        public hasCommand(name: string): boolean {
            return this._commandMap.has(name);
        }
        /**
         *@inheritDoc
         */
        public executeCommand(name: string): void
        {
            if(!this.hasCommand(name))
                throw new Error("Error execute not registered command.");
            let cmd:ICommand;

            if (this._commandsInMemory.has(name))
            {
                cmd = this._commandsInMemory.get(name);
            }
            else
            {
                let cmdConstructor:any = this._commandMap.get(name);
                cmd = new cmdConstructor(this.appContext.getModel(),this.appContext.getView());
                if (cmd.IsStayInMemory)
                    this._commandsInMemory.set(name,cmd);
            }

            cmd.execute();



        }

        /**
         * Remove command by name
         * @param {string} name existen registered command name
         */
        public removeCommand( name:string ): void
        {
            if(this.hasCommand(name))
            {
                if(this._commandsInMemory.has(name))
                {
                    this._commandsInMemory.get(name).dispose();
                    this._commandsInMemory.delete(name);
                }
                this._commandMap.delete( name );
            }
        }
        /**
         * Dispose allocated data.
         */
        public dispose():void
        {
            if(this._commandsInMemory!=null)
            {

                for( var mapKey in this._commandsInMemory )
                {
                    this._commandsInMemory.get(mapKey).dispose();
                    this._commandsInMemory.delete(mapKey);
                }
            }
        }
    }
}