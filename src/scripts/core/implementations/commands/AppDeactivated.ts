namespace core.implementations.commands
{
    export class AppDeactivated extends BaseCoreCommand
    {
        /**
         * @inheritDoc
         */
        public execute(): void
        {
            this.model.isAppActive = false;
            this.view.setEnabled(false);
        }
    }
}