namespace core.implementations.commands
{
    export class AppResizeCommand extends BaseCoreCommand
    {
        /**
         * @inheritDoc
         */
        execute()
        {
           let element:HTMLElement = document.getElementById(this.model.canvasHTMLElementDivName);

           //Do resize, calculate scales and etc.
           this.model.canvasResize(element.offsetWidth, element.offsetHeight);

           //Resize app view
            this.view.resize(this.model.canvasWidth, this.model.canvasHeight);
        }
    }
}