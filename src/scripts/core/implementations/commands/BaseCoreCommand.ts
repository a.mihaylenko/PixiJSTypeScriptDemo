namespace core.implementations.commands
{
    export class BaseCoreCommand extends AbstractCommand
    {
        /**
         * @inheritDoc
         */
        constructor(model: IModel, view: IView)
        {
            super(model,view);
            let t:string = typeof this;

            if (t == "BaseCoreCommand")
                throw new Error("BaseCoreCommand can't an instance, because this is a base class, please use his extend classes for instance. ");
        }
        private _view:IView;
        /**
         * @inheritDoc
         */
        protected get view(): IView{return this._view;}
        /**
         * @inheritDoc
         */
        protected set view(value:IView){this._view = value;}

        private _model:IModel;
        /**
         * @inheritDoc
         */
        protected get model(): IModel {return this._model;}
        /**
         * @inheritDoc
         */
        protected set model(value:IModel){this._model=value;}

        /**
         * @inheritDoc
         */
        public execute(): void{}
        /**
         * @inheritDoc
         */
        public dispose(): void
        {
            this._view=null;
            this._model=null;
        }

        public get IsStayInMemory(): boolean{return true;}
    }
}