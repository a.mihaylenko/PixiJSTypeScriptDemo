namespace  core.implementations.commands
{
    export class AppCoreViewRedrawCommand extends BaseCoreCommand
    {
        execute():void
        {
            this.view.onEnterFrameRedraw();
        }
    }
}