namespace core.implementations.commands
{
    export const APP_ACTIVATED:string   =   "core.cmd.APP_ACTIVATED";
    export const APP_RESIZE_CMD:string  =   "core.cmd.APP_RESIZE_CMD";
    export const APP_DEACTIVATED:string =   "core.cmd.APP_DEACTIVATED";
    export const APP_VIEW_REDRAW:string =   "core.cmd.APP_VIEW_REDRAW";

}