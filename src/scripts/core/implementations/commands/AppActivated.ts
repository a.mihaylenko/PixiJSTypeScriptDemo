///<reference path="BaseCoreCommand.ts"/>
namespace core.implementations.commands
{
    export class  AppActivated extends BaseCoreCommand
    {
        /**
         * @inheritDoc
         */
        public execute(): void
        {
           this.model.isAppActive = true;
        }

    }
}