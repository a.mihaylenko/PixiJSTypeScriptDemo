declare namespace core.implementations {
    import IAppContext = core.interfaces.IAppContext;
    import IModel = core.interfaces.IModel;
    import IView = core.interfaces.IView;
    import IController = core.interfaces.IController;
    abstract class AbstractAppContext implements IAppContext {
        enterFrameUpdate(): void;
        abstract getModel(): IModel;
        abstract getView(): IView;
        abstract getController(): IController;
        protected registercoreCommands(): void;
    }
}
