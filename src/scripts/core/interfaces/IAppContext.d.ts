declare namespace core.interfaces {
    interface IAppContext {
        getModel(): IModel;
        getView(): IView;
        getController(): IController;
        enterFrameUpdate(): void;
    }
}
