declare namespace core.interfaces {
    interface IModel {
        canvasResize(width: number, height: number): void;
        getCanvasSize(): Point;
        getCanvasElementName(): string;
        isAppActive: boolean;
    }
}
