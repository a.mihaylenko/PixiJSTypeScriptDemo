declare namespace core.interfaces {
    interface IController extends IDisposable {
        registerCommand(name: string, classImpl: Function): void;
        hasCommand(name: string): boolean;
        executeCommand(name: string, stayInMemory: boolean): void;
        removeComand(name: string): void;
    }
}
