namespace core.interfaces {
    export interface ICommand extends IDisposable {
        IsStayInMemory: boolean;
        execute(): void;

    }
}
