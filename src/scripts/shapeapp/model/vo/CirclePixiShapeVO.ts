///<reference path="AbstractPixiShapeVO.ts"/>

namespace shapeapp.model.vo
{
    import ShapeTypes = shapeapp.Enums.ShapeTypes;

    export class CirclePixiShapeVO extends AbstractPixiShapeVO
    {
        protected _maxRadius:number = 50;
        protected _minRadius:number = 10;
        protected _radius:number;
        protected _shape:PIXI.Circle;
        private _surfaceArea:number;
        constructor(radius?:number|undefined)
        {
            super();
            this.generateShape(radius);
            this.randomizeColors();
            this.lineWidth = 1 + Math.random()*this._radius/3;

        }
        protected generateShape( radius?:number|undefined )
        {
            this._radius = typeof radius!="undefined"?radius:(this._minRadius + Math.random()*(this._maxRadius-this._minRadius));
            this._shape = new PIXI.Circle(0,0,this._radius);

            this._surfaceArea = Math.PI*this._radius*this._radius;
        }
        public get surfaceArea():number
        {
            return this._surfaceArea;
        }

        public get shape():PIXI.Circle
        {
            return this._shape;
        }
        public get shapeType():ShapeTypes
        {
            return ShapeTypes.Circle;
        }

        public clone(withRundomValue:boolean = true): AbstractPixiShapeVO
        {

            var clone = this.generateCloneInstance(withRundomValue);
            if(withRundomValue)
            {
                clone.randomizeColors();
                clone.lineWidth = 1 + Math.random()*clone._radius/3;
            }
            else {
                clone.fillColor = this.fillColor;
                clone.fillAlpha = this.fillAlpha;

                clone.lineColor = this.lineColor;
                clone.lineAlpha = this.lineAlpha;

                clone.position.copy(this.position);
            }


            return clone;
        }
        protected generateCloneInstance(withRundomValue:boolean = true):CirclePixiShapeVO
        {
            return  new CirclePixiShapeVO(withRundomValue?undefined:this._radius);
        }
    }
}