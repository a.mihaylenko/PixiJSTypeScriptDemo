namespace shapeapp.model.vo
{
    import ShapeTypes = shapeapp.Enums.ShapeTypes;
    import Ellipse = PIXI.Ellipse;

    export class EllipseShapeVO extends AbstractPixiShapeVO
    {
        private _maxSize:number=50;
        private _minSize:number=10;

        private _width:number;
        private _height:number;

        private _surfaceArea:number;

        constructor(width?:number, height?:number)
        {
            super();
            this.createShape(width,height);
            this.randomizeColors();
            this.lineWidth = 1 + Math.random()*this._width/3;
        }
        public get surfaceArea():number
        {
            return this._surfaceArea;
        }
        private _shape:Ellipse
        public get shape():Ellipse
        {
            return this._shape;
        }
        public get shapeType():ShapeTypes
        {
            return ShapeTypes.Ellipse;
        }

        public clone( withRundomValue:boolean ):AbstractPixiShapeVO
        {
            var clone:EllipseShapeVO = new EllipseShapeVO(withRundomValue?undefined:this._width, withRundomValue?undefined:this._height );
            if(withRundomValue)
            {
                clone.randomizeColors();
                clone.lineWidth = 1 + Math.random()*clone._width/3;
            }
            else {
                clone.fillColor = this.fillColor;
                clone.fillAlpha = this.fillAlpha;

                clone.lineColor = this.lineColor;
                clone.lineAlpha = this.lineAlpha;

                clone.position.copy(this.position);
            }

            return clone;
        }

        private createShape(width?:number, height?:number):void
        {
            this._width = typeof width== "undefined" ?this.rndValue():width;
            this._height = typeof height== "undefined"?this.rndValue():height;

            this._shape = new Ellipse(0, 0, this._width, this._height);

            this._surfaceArea = Math.PI*this._width*this._height;
        }

        private rndValue():number
        {
            return this._minSize + Math.random()*(this._maxSize-this._minSize);
        }

    }
}