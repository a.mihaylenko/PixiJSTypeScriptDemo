namespace shapeapp.controller.commands
{
    export class GravityCountDecrease extends  BaseShapeAppCommand
    {
        execute():void
        {
            if (this.model.gravityVal>.1) {
                this.model.gravityVal -= .5;
                this.view.updateGravityValue(this.model.gravityVal);
                document.getElementById("GravityCountClickText").innerText = "Gravity:" + this.model.gravityVal.toFixed(1).toString();
            }
        }
    }
}