namespace shapeapp.controller.commands
{
    export class ShapesCountDecteaseAtClickCmd extends BaseShapeAppCommand
    {
        public execute():void
        {
            if(this.model.multiplierOfCreation>1)
            {
                this.model.multiplierOfCreation-=1;
                document.getElementById("ShapesCountAtClickText").innerText = "Shapes count:" + this.model.multiplierOfCreation;
            }
        }
    }
}