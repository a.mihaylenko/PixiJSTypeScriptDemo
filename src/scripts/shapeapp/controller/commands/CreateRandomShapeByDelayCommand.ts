///<reference path="DelayedShapeCommand.ts"/>
namespace shapeapp.controller.commands
{
    import Point = PIXI.Point;

    export class CreateRandomShapeByDelayCommand extends DelayedShapeCommand
    {
        protected delayedExecute(): void
        {
            var indexesForRemove:number[] = this.view.getClickedOnShapesIndexesAndSetEmpty();
            var positionsForNewShapes:Point[] = this.view.getPositionsForNewShapesAndSetEmpty();

            var hasAnyChanges:Boolean=false;
            if(indexesForRemove.length>0)
            {
                this.model.totalSurfaceArea-=this.view.removeShapesByIndexes(indexesForRemove);

                hasAnyChanges=true;
            }

            if(positionsForNewShapes.length>0)
            {
                this.model.generateRandomShapesByPositions(positionsForNewShapes);
                this.model.totalSurfaceArea+=this.view.drawNewShapes(this.model.generatedShapesToDraw);
                this.model.clearGeneratedShapes();
                hasAnyChanges= true;
            }

            if(hasAnyChanges) {
                this.view.updateNumberOfShapesHTMLDivInnerText(this.model.canvasNumberShapesDivName);
                this.view.updateTotalSurfaceAreaHTMLDivInnerText(this.model.canvasTotalSurfaceAreaDivName, Math.max(0,this.model.totalSurfaceArea));
            }
        }

        protected get delay():number
        {
            return this.model.delayForCreateRndShape;
        }

    }
}