namespace shapeapp.controller.commands
{
    export class ShapesCountIncreaseAtClickCmd extends BaseShapeAppCommand
    {
        public execute():void
        {
            this.model.multiplierOfCreation+=1;
            document.getElementById("ShapesCountAtClickText").innerText = "Shapes count:" + this.model.multiplierOfCreation;
        }
    }
}