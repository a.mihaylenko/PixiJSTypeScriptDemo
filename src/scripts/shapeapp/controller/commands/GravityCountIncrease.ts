namespace shapeapp.controller.commands
{
    export class GravityCountIncrease extends  BaseShapeAppCommand
    {
        execute():void
        {
            this.model.gravityVal += .5;
            this.view.updateGravityValue(this.model.gravityVal);
            document.getElementById("GravityCountClickText").innerText = "Gravity:" + this.model.gravityVal.toFixed(1).toString();
    }
    }
}