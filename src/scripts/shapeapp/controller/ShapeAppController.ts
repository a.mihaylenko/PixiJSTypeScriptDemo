import AbstractController = core.implementations.AbstractController;

namespace shapeapp.controller
{

    export class ShapeAppController extends AbstractController
    {

        constructor(context:ShapeAppContext)
        {
            super();
            this._context = context;
        }

        private _context:ShapeAppContext;
        protected get appContext(): ShapeAppContext
        {
            return this._context;
        }
        protected set appContext(value: ShapeAppContext)
        {
            this._context = value;
        }

    }
}