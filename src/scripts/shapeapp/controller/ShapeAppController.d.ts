declare namespace shapeapp.controller {
    import AbstractController = core.implementations.AbstractController;
    import IAppContext = core.interfaces.IAppContext;
    class ShapeAppController extends AbstractController {
        _appContext: IAppContext;
        constructor(context: IAppContext);
    }
}
