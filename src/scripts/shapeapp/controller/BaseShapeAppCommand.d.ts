declare namespace shapeapp.controller {
    import PixiEngineView = shapeapp.view.PixiEngineView;
    import AbstractCommand = core.implementations.AbstractCommand;
    class BaseShapeAppCommand extends AbstractCommand {
        execute(): void;
        dispose(): void;
        protected GetShapeAppView(): PixiEngineView;
    }
}
